
<?php

Route::get('/', "HomeController@index");

Route::get('contacts', "ContactsController");

Route::get('contact/{id}', "ContactController@getContact");

Route::post('contact', "ContactController@newContact");

Route::delete('contact/{id}', "ContactController@delContact");
Route::resource('student', 'StudentController');
