@extends('layouts.master')
@section('content')

<div class="row">
<div class="col-md-12">
  <br/>
<h3 align="center">Add Contact</h3>    
<br/>
@if(count($errors) >0) 
    <ul>
        @foreach($errors->all() as $error)
    <li>{{$error}}</li>
        @endforeach
    </ul>
</div>
    @endif
    @if(\Session::has('success'))
    <div class="alert alert-success">
    <p>{{ \Session::get('success')}}</p>
        
</div>
@endif
    <form method="post" action="{{url('contacts')}}">
    {{csrf_field()}}
        <div class="form-group">
        <input type="text" name="first_name" class="form-control" placeholder="First name">
        </div>
        <div class="form-group">
        <input type="text" name="last_name" class="form-control" placeholder="Last name">
        </div>
        <div class="form-group">
        <input type="email" name="email" class="form-control" placeholder="Email">
        </div>
        <div class="form-group">
        <input type="tel" name="cell" class="form-control" placeholder="Phone number">
        </div>
        <div class="form-group">
        <input type="submit" class="btn btn-primary" value="save">
        </div>
    
    </form>
</div>
   
@endsection
